package com.molyfun.bean.student;


import java.util.Date;
public class MhStudentBean {
   
    private String account;
    private Date birthdatetime;
    private String comefrom;
    private Date createdatetime;
    private String departid;
    private String diagnosed;
    private String diagnostic;
    private String headimgurl;
    private String iq;
    private String name;
    private String nation;
    private String phone;
    private String sex;
    private String userid;
    private String zipcode;
    private String delflag;
    private String age;
    private String cardId;
    private String contacts;
    private String password;
    private String token;
    private String address;
    private String situationdescription;

    //邀请码
    private String inviteCode;
    
  
	
	
	
	

	public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

   
    public Date getBirthdatetime() {
        return birthdatetime;
    }

    public void setBirthdatetime(Date birthdatetime) {
        this.birthdatetime = birthdatetime;
    }

    public String getComefrom() {
        return comefrom;
    }
    public void setComefrom(String comefrom) {
        this.comefrom = comefrom == null ? null : comefrom.trim();
    }

   
    public Date getCreatedatetime() {
        return createdatetime;
    }

    public void setCreatedatetime(Date createdatetime) {
        this.createdatetime = createdatetime;
    }

    public String getDepartid() {
        return departid;
    }

    public void setDepartid(String departid) {
        this.departid = departid == null ? null : departid.trim();
    }

    public String getDiagnosed() {
        return diagnosed;
    }
    public void setDiagnosed(String diagnosed) {
        this.diagnosed = diagnosed == null ? null : diagnosed.trim();
    }

    public String getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(String diagnostic) {
        this.diagnostic = diagnostic == null ? null : diagnostic.trim();
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl == null ? null : headimgurl.trim();
    }

    public String getIq() {
        return iq;
    }

    public void setIq(String iq) {
        this.iq = iq == null ? null : iq.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation == null ? null : nation.trim();
    }

   
    public String getPhone() {
        return phone;
    }

  
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

   
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

   
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid == null ? null : userid.trim();
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode == null ? null : zipcode.trim();
    }

    public String getDelflag() {
        return delflag;
    }

    public void setDelflag(String delflag) {
        this.delflag = delflag == null ? null : delflag.trim();
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age == null ? null : age.trim();
    }

    public String getCardId() {
        return cardId;
    }

  
    public void setCardId(String cardId) {
        this.cardId = cardId == null ? null : cardId.trim();
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts == null ? null : contacts.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token == null ? null : token.trim();
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }
    public String getSituationdescription() {
        return situationdescription;
    }
    public void setSituationdescription(String situationdescription) {
        this.situationdescription = situationdescription == null ? null : situationdescription.trim();
    }
   
    public String getInviteCode() {
		return inviteCode;
	}

	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}

}